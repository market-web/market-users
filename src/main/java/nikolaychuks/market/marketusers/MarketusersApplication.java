package nikolaychuks.market.marketusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketusersApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketusersApplication.class, args);
    }

}
